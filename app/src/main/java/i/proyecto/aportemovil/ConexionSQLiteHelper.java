package i.proyecto.aportemovil;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import i.proyecto.aportemovil.utilidades.UtilidadesConserje;

public class ConexionSQLiteHelper extends SQLiteOpenHelper {
   // final String CREAR_TABLA_ESTUDIANTE ="CREATE TABLE estudiantes (id INTEGER, nombre TEXT, carrera TEXT,nivel TEXT)";
    public ConexionSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(UtilidadesConserje.CREAR_TABLA_CONSERJE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS conserjes");
        onCreate(db);
    }
}
