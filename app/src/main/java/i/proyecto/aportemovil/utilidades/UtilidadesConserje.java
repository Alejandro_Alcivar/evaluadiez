package i.proyecto.aportemovil.utilidades;

public class UtilidadesConserje {
    public static  final String TABLA_CONSERJE ="conserje";
    public static  final String CAMPO_ID ="id";
    public static  final String CAMPO_NOMBRE ="nombre";
    public static  final String CAMPO_APELLIDO ="apellido";
    public static  final String CAMPO_FECHANA ="fechana";
    public static  final String CAMPO_FACTURA ="facutra";

    public static  final String CREAR_TABLA_CONSERJE ="CREATE TABLE "+
            TABLA_CONSERJE+" ("+
            CAMPO_ID+" INTEGER, "+
            CAMPO_NOMBRE+" TEXT, "+
            CAMPO_APELLIDO+" TEXT, "+
            CAMPO_FECHANA+" TEXT)"+
            CAMPO_FACTURA+"TEXT";
}
