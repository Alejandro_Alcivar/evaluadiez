package i.proyecto.aportemovil.entidades;

public class Conserje {
    private Integer id;
    private String nombre;
    private String apellido;
    private String fechana;
    private String facutra;


    public Conserje (Integer id, String nombre, String apellido, String fechana, String facutra) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechana = fechana;
        this.facutra = facutra;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getFechana() {
        return fechana;
    }

    public void setFechana(String fechana) {
        this.fechana = fechana;
    }

    public String getFacutra() {
        return facutra;
    }

    public void setFacutra(String facutra) {
        this.facutra = facutra;
    }
}
