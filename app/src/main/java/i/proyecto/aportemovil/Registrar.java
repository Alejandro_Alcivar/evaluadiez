package i.proyecto.aportemovil;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import i.proyecto.aportemovil.utilidades.UtilidadesConserje;


public class Registrar extends AppCompatActivity {
    EditText textNombres, textApellidos, textFechanaci, textFacutra, textId;
    Button buttonRegistrar;
    ConexionSQLiteHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        conn = new ConexionSQLiteHelper(this, "bd estudiantes", null, 1);

        textNombres = findViewById(R.id.editTextNombre);
        textApellidos = findViewById(R.id.apellidos);
        textFechanaci = findViewById(R.id.fechaNaci);
        textFacutra = findViewById(R.id.facuTra);
        textId = findViewById(R.id.editTextId);


        buttonRegistrar = findViewById(R.id.buttonRegistrar);


        buttonRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarConserje();
            }
        });

    }

    private void registrarConserje() {
        SQLiteDatabase db = conn.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UtilidadesConserje.CAMPO_ID,textId.getText().toString());
        values.put(UtilidadesConserje.CAMPO_NOMBRE,textNombres.getText().toString());
        values.put(UtilidadesConserje.CAMPO_APELLIDO,textApellidos.getText().toString());
        values.put(UtilidadesConserje.CAMPO_FECHANA,textFechanaci.getText().toString());
        values.put(UtilidadesConserje.CAMPO_FACTURA,textFacutra.getText().toString());
        Long idResultatnte= db.insert(UtilidadesConserje.CREAR_TABLA_CONSERJE,UtilidadesConserje.CAMPO_ID,values);
        Toast.makeText(getApplicationContext(),"Registro id:"+idResultatnte,Toast.LENGTH_LONG).show();
        db.close();


    }
}

