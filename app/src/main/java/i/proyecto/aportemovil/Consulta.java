package i.proyecto.aportemovil;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import i.proyecto.aportemovil.utilidades.UtilidadesConserje;

public class Consulta extends AppCompatActivity {
    EditText editTextCedula;
    Button buttonCedula;
    TextView textViewCedula,textViewNombre,textViewApellidos,textViewFechana, textViewFacutra;


    ConexionSQLiteHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta);
        conn = new ConexionSQLiteHelper(this,"bd estudiantes",null,1);
        editTextCedula = findViewById(R.id.editTextCedula);
        buttonCedula = findViewById(R.id.buttonCedula);
        textViewNombre = findViewById(R.id.textViewNombre);
        textViewApellidos = findViewById(R.id.textViewApellidos);
        textViewFechana = findViewById(R.id.textViewFechana);
        textViewFacutra = findViewById(R.id.textViewFacutra);




        buttonCedula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                consultar();
            }
        });

    }

    private void consultar() {
        SQLiteDatabase db = conn.getReadableDatabase();
        String[] parametro = {editTextCedula.getText().toString()};
        String[] campo = {UtilidadesConserje.CAMPO_ID,UtilidadesConserje.CAMPO_NOMBRE,
                UtilidadesConserje.CAMPO_APELLIDO,UtilidadesConserje.CAMPO_FECHANA,UtilidadesConserje.CAMPO_FACTURA};

        try{
            Cursor cursor = db.query(UtilidadesConserje.TABLA_CONSERJE,campo,UtilidadesConserje.CAMPO_ID+"=?",parametro,null,null,null);
            cursor.moveToFirst();
            textViewCedula.setText(cursor.getString(0));
            textViewNombre.setText(cursor.getString(1));
            textViewApellidos.setText(cursor.getString(2));
            textViewFechana.setText(cursor.getString(3));
            textViewFacutra.setText(cursor.getString(4));
            cursor.close();
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"El id no existe",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Consulta.this, Registrar.class);
            startActivity(intent);
        }
        db.close();

    }
}

